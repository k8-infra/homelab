#!/bin/bash
if [ -z ${ARGOCD_NS+x} ];then
  ARGOCD_NS='argocd'
fi

echo "INFO: Argocd will be installed on $ARGOCD_NS namespace"
echo -n "Do you want to proceed? [y/n]: "
read ans
if [ "$ans" == "y" ]; then
  helm repo add argo https://argoproj.github.io/argo-helm
  helm repo update
  export ARGOCD_EXEC_TIMEOUT=300
  helm upgrade --install argocd argo/argo-cd \
    --namespace=$ARGOCD_NS \
    --create-namespace \
    -f ./argo-values.yaml
  SECRET=$(kubectl get secret -n argocd argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d)
  echo "initial pw is: $SECRET"
  while [[ $(kubectl get pods $NAME -n argocd -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; 
    do 
       sleep 60;
    done
  kubectl port-forward service/argocd-server -n argocd 8080:80 &
else
  echo "INFO: Exit without any action"
  exit 0
fi