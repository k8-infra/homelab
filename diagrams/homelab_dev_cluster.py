from diagrams import Cluster, Diagram
from diagrams.k8s.infra import Master
from diagrams.k8s.infra import Node
from diagrams.k8s.storage import PVC
from diagrams.k8s.controlplane import API
from diagrams.onprem.compute import Server
from diagrams.generic.storage import Storage


with Diagram("Homelab Dev Cluster", show=False):
    Node01 = Server("Node01")
    with Cluster("Storage"):
       HDD01 = Storage("HDD01")
       HDD02 = Storage("HDD02")

    with Cluster("Kubevirt Dev Cluster"):

       with Cluster("Control Plane"):
            masters = [
              Master("master01"),
              Master("master02"),
              Master("master03"),
            ]

            master_storage = [
              PVC("master01"),
              PVC("master01"),
              PVC("master01")
            ]
       apiserver = API("Master API")

       with Cluster("Data Plane"):
          workers = [
            Node("worker01"),
            Node("worker02"),
            Node("worker03")
          ]
          worker_storage = [
            PVC("worker01"),
            PVC("worker02"),
            PVC("worker03")
          ]
    
    HDD01 >> master_storage
    HDD02 >> worker_storage
    Node01 >> masters >> apiserver >> workers
